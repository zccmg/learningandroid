import java.util.ArrayList;

public class Guri implements ItemsInterface {

    private ArrayList<Items> itemises = new ArrayList<>();

    @Override
    public void AddItem(Items items) {
        itemises.add(items);
    }

    @Override
    public void RemoveItem(Items items) {
        itemises.remove(items);
    }

    @Override
    public ArrayList<Items> getAll() {
        return itemises;
    }
}
